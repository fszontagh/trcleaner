# TRCleaner

Interactive file cleaner for transmission daemon.
Removes all files from the download paths, what is not found in the Transmission Daemon. 

##INSTALL
Clone the repo: 
git clone https://bitbucket.org/fszontagh/trcleaner.git

### Edit the 'transmissionDelete.php' file:
-  Change the 'MYPATH' constant in line #5
-  Change RCP username/password in the line #16. 
-  Run the script, it will ask for deletions
-  You can force to delete all unused files, without asking: transmissionDelete.php --force


## Transmission client class:
> Transmission bittorrent client/daemon RPC communication class   
> Copyright (C) 2010 Johan Adriaans <johan.adriaans@gmail.com>,
>                      Bryce Chidester <bryce@cobryce.com>
					 
> For full icence, please see: class/transmission.class.php header.