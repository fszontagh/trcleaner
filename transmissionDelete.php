#!/usr/bin/php
<?php
$user = posix_getpwuid(posix_getuid());
if (!isset($user['dir'])) {
    echo "ERROR: home directory not found!" . PHP_EOL;
    exit;
}

echo "Hi " . $user['name'] . "!" . PHP_EOL . PHP_EOL;
$config_file_name = $user['dir'] . DIRECTORY_SEPARATOR . ".config/trcleaner.ini";
$memory_path = $user['dir'] . DIRECTORY_SEPARATOR . ".config/trcleaner/";
if (!file_exists($memory_path)) {
    mkdir($memory_path);
}

//get the scrip's path
$myDir = dirname(__FILE__);


$config = [
    "hostname"  => 'localhost',
    "port"      => 9091,
    "path"      => "/transmission/rpc",
    "ssl"       => false,
    "username"  => null,
    "password"  => null,
    "data_path" => null
];


if (!file_exists($config_file_name) OR (isset($argv[1]) AND $argv[1] == "--reconf")) {
    if (file_exists($config_file_name)) {
        $config_ = parse_ini_file($config_file_name, false, INI_SCANNER_TYPED);
        $config = array_merge($config, $config_);
    }
    $ans = readline("RPC hostname/ip [" . $config['hostname'] . "]: ");
    $tr_hostname = trim($ans);
    if (strlen($tr_hostname) > 1) {
        $config['hostname'] = $tr_hostname;
    }

    $ans = readline("RPC port [" . $config['port'] . "]: ");
    $tr_port = intval($ans);
    if (strlen($tr_port) > 1) {
        $config['port'] = $tr_port;
    }

    $ans = readline("RPC path [" . $config['path'] . "]: ");
    $tr_path = trim($ans);

    if (strlen($tr_path) > 2) {
        $config['path'] = $tr_path;
    }

    $ans = strtolower(trim(readline("Use SSL (https) [yes/no - default: " . ($config['ssl'] === true ? "yes" : "no") . "]? ")));

    if ($ans === "yes" OR $ans === "y") {
        $config['ssl'] = true;
    } else {
        $config['ssl'] = false;
    }


    $ans = readline("RPC username (press enter, when no auth): ");

    if (empty(trim($ans)) === false) {
        $tr_username = trim($ans);
        $ans = readline("RPC password: ");
        $tr_password = trim($ans);
        $config['username'] = $tr_username;
        $config['password'] = $tr_password;
    }

    echo "Config saved... " . PHP_EOL;
    touch($config_file_name);
    saveConfig($config_file_name, $config);
    $old_data_path = $config['data_path'];
    $config['data_path'] = null;
} else {
    $config = parse_ini_file($config_file_name, false, INI_SCANNER_TYPED);
}

while ($config['data_path'] === null) {
    $ans = trim(readline("Path to the torrent data" . (isset($old_data_path) ? " [" . $old_data_path . "]" : "") . ": "));
    if (isset($old_data_path) AND strlen(trim($ans)) < 2) {
        $config['data_path'] = $old_data_path;
    }
    if (strlen($ans) > 2 AND file_exists($ans)) {
        $config['data_path'] = $ans;
        saveConfig($config_file_name, $config);
    }
    if (file_exists($config['data_path']) === false) {
        echo "Data path not found!" . PHP_EOL;
        $config['data_path'] = null;
    } else {
        echo "Data path saved!" . PHP_EOL;
        usleep(500);
    }
}


include $myDir . DIRECTORY_SEPARATOR . 'class/transmission.class.php';


$tr_full_path = ($config['ssl'] === true ? "https://" : "http://") . $config['hostname'] . ":" . $config['port'] . "" . $config['path'];

if ($config['username'] !== null) {
    $tr = new TransmissionRPC($tr_full_path, $config['username'], $config['password']);
} else {
    $tr = new TransmissionRPC($tr_full_path);
}

$session_info = $tr->sget();
$download_dir = $session_info->arguments->download_dir;

echo "Transmission download dir: " . $download_dir . PHP_EOL;
echo "Local data dir: " . $config['data_path'] . PHP_EOL;

$list = $tr->get(array(), array("files", "name", "downloadDir", "totalSize"));

if (isset($list->result) AND $list->result == 'success') {
    echo "Connection ok!" . PHP_EOL;
    usleep(1000);
} else {
    echo "Can not connect to the transmission daemon: " . str_replace([
        $config['username'],
        $config['password']
            ], [
        str_repeat("*", strlen($config['username'])),
        str_repeat("*", strlen($config['username']))
            ], $tr_full_path) . PHP_EOL;
    exit;
}


$force_del = false;
if (isset($argv[1]) AND $argv[1] == "--force") {


    echo "**WARNING**\nForce del activated... Do you really want to delete all files without questions? ";
    $ans = readline("YES/NO (Def.: no):");
    if ($ans === "YES") {
        $force_del = true;

        echo "\n\n\t\t******FORCE DELETE ACTIVATED******\n";
    } else {
        echo "\n\n\t\t****I will not delete without question...****\n";
    }
    echo "\n\n";
}


echo "\t Generating used file list from TR:  0";

$tr_files = array();
$paths2scan = array();
$i = 0;
$sizes = 0;
foreach ($list->arguments->torrents as $id => $torrent) {

    $sizes += $torrent->totalSize;

    foreach ($torrent->files as $fid => $file) {


        $downdir = $torrent->downloadDir;

        $downdir = str_replace($download_dir, $config['data_path'], $downdir);

        if (substr($downdir, -1) === DIRECTORY_SEPARATOR) {
            $downdir = substr($downdir, 0, -1);
        }

        $tr_files[md5($downdir . DIRECTORY_SEPARATOR . $torrent->name)] = $downdir . DIRECTORY_SEPARATOR . $torrent->name;
        $tr_files[md5($downdir . DIRECTORY_SEPARATOR . $file->name)] = $torrent->downloadDir . DIRECTORY_SEPARATOR . $file->name;


        $paths2scan[md5($downdir)] = $downdir;


        echo "\r\t Generating used file list from TR:  " . $i . " & Path to scan: " . count($paths2scan) . " (" . $torrent->downloadDir . ") | loading..." . str_repeat(" ", 25);
        $i++;
    }
}
echo "\r\t Generated used file list from TR:  " . $i . " & Path to scan: " . count($paths2scan) . " Size: " . formatBytes($sizes) . "| done!" . str_repeat(" ", 25);
echo "\n";


file_put_contents($memory_path . DIRECTORY_SEPARATOR . "torrent_files.txt", print_r($tr_files, true));
echo "File list saved into: " . $memory_path . DIRECTORY_SEPARATOR . "torrent_files.txt" . PHP_EOL;

echo "\n\n\tThese download paths will be scanned for trash: \n";

$globs = array();
$i = 0;
foreach ($paths2scan as $ihash => $path) {
    echo "\t\tScanning dir: " . $path . " \t\t" . $i . "\r";
    $globs = array_merge($globs, __scanDir($path, $i, $path));
    echo "\n";
    usleep(500);
}



echo "\n Comparing files to TR file list... please wait...\n";

foreach ($globs as $id => $path) {
    if (!isset($tr_files[$id])) {
        if (is_dir($path) AND file_exists($path)) {

            echo str_repeat("-", 80) . "\n";

            echo $path . "\t \n\r";


            listDir($path);
            echo str_repeat("-", 80) . "\n";
            if ($force_del === false) {
                $r = readline("Delete? [y/N]: ");
                if (strtolower(trim($r)) === 'y') {
                    rrmdir($path);
                    echo "done!\n";
                } else {
                    echo "SKIPPED!!\n";
                }
            } else {
                if (rrmdir($path) === true) {
                    echo "*DELETED " . $path . "\n";
                }
            }
        }
    }
}

function listDir($dir) {

    if (is_file($dir)) {
        echo $dir . PHP_EOL;
    } else {
        $scanned_directory = array_diff(scandir($dir), array('..', '.'));

        foreach ($scanned_directory as $id => $f) {
            echo $dir . DIRECTORY_SEPARATOR . $f . PHP_EOL;
        }
    }
}

function rrmdir($t) {
    if (is_file($t)) {
        return unlink($t);
    }
    if (is_dir($t)) {
        $scanned_directory = array_diff(scandir($t), array('..', '.'));

        foreach ($scanned_directory as $i => $p) {
            rrmdir($t . DIRECTORY_SEPARATOR . $p);
        }
        echo "Deleted: " . $t . "\n";
        rmdir($t);
    }
}

function __scanDir($path, $i, $mpath) {
    $g = glob($path . "/*");
    $ps = array();
    $i2 = 0;
    foreach ($g as $p) {
        $ps[md5($p)] = $p;
        if (is_file($p)) {
            $i++;
            $i2++;
            usleep(600);
        } else {
            $ps = array_merge($ps, __scanDir($p . "/*", $i, $mpath));
        }
    }
    return $ps;
}

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow];
}

function saveConfig($file, $array) {
    $res = array();
    foreach ($array as $key => $val) {
        if (is_array($val)) {
            $res[] = "[$key]";
            foreach ($val as $skey => $sval) {
                $res[] = "$skey = " . __getType($sval);
            }
        } else {
            $res[] = "$key = " . __getType($val);
        }
    }
    saveFile($file, implode("\r\n", $res));
}

function __getType($s) {
    if (is_numeric($s)) {
        return intval($s);
    }
    if (is_bool($s)) {
        return $s === true ? "true" : "false";
    }
    if (is_numeric($s)) {
        return intval($s);
    }
    if (is_null($s) OR strtolower($s) == "null") {
        return null;
    }
    if (is_string($s)) {
        $s = str_replace("\"", "\\\"", $s);
        return '"' . $s . '"';
    }
}

function saveFile($fileName, $dataToSave) {
    if ($fp = fopen($fileName, 'w')) {
        $startTime = microtime(TRUE);
        do {
            $canWrite = flock($fp, LOCK_EX);
            // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
            if (!$canWrite)
                usleep(round(rand(0, 100) * 1000));
        } while ((!$canWrite)and((microtime(TRUE) - $startTime) < 5));

        //file was locked so now we can store information
        if ($canWrite) {
            fwrite($fp, $dataToSave);
            flock($fp, LOCK_UN);
        }
        fclose($fp);
    }
}
?>
